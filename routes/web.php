<?php

use App\Http\Controllers\GameController;
use App\Http\Controllers\PlayerController;
use App\Http\Controllers\RoundController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\TournamentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return redirect(route('tournaments.index'));
    })->name('dashboard');

    Route::put('players/{player}/toggle', [PlayerController::class, 'toggle'])->name('players.toggle');
    Route::delete('players/{player}', [PlayerController::class, 'destroy'])->name("players.destroy");

    Route::resource('tournaments', TournamentController::class);
    
    Route::get('tournaments/{tournament}/start', [TournamentController::class, 'start'])->name('tournaments.start');
    Route::get('tournaments/{tournament}/round/next', [TournamentController::class, 'nextRound'])->name('tournaments.round.next');
    Route::post('tournaments/{tournament}/player', [TournamentController::class, 'storePlayer'])->name('tournaments.player.store');
    Route::get('rounds/{round}/reset', [RoundController::class, 'reset'])->name('rounds.reset');
    Route::get('tournaments/{tournament}/round/{round:iteration}', [RoundController::class, 'show'])->name('tournaments.round');
    Route::get('tournaments/{tournament}/close', [TournamentController::class, 'close'])->name('tournaments.close');
    Route::get('tournaments/{tournament}/ranking', [TournamentController::class, 'ranking'])->name('tournaments.ranking');
    Route::put('games/{game}/squad/{squad}', [GameController::class, 'updateScore'])->name('game.update.score');
});
