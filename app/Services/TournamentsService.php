<?php

namespace App\Services;

use App\Models\Game;
use App\Models\Round;
use App\Models\Squad;
use App\Models\Tournament;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class TournamentsService
{
    public function index()
    {
        $user = Auth::user();
        return $user->tournaments;
    }

    public function start(Tournament $tournament)
    {
        $round = new Round();
        $round->iteration = 1;

        $tournament->rounds()->save($round);
        $this->populate_round($round);

        return $round;
    }

    public function nextRound(Tournament $tournament)
    {
        // On lock le round à clôturer
        $current_round = $tournament->rounds->last();
        $current_round->lock = true;
        $current_round->save();


        $round = new Round();
        $round->iteration = $current_round->iteration + 1;
        $tournament->rounds()->save($round);
        $this->populate_round($round);

        return $round;
    }

    public function close(Tournament $tournament)
    {
        $tournament->rounds()->update([
            'lock' => true
        ]);
            
        $tournament->finished = true;
        $tournament->save();
        
        return $tournament;
    }

    public function populate_round(Round $round)
    {
        $tournament = $round->tournament;
        $squads = $this->make_squads($tournament);
        $round->squads()->saveMany($squads);
        $round->refresh();
        $this->make_games($round);

        return $round;
    }

    private function make_games(Round $round)
    {
        $squads = $round->squads;
        $nbGame = 1;
        for ($i = 0; $i < $squads->count(); $i += 2) {
            $game = new Game([
                "name" => "Match $nbGame"
            ]);
            $game = $round->games()->save($game);

            $game->squads()->attach([
                $squads[$i]->id,
                $squads[$i + 1]->id
            ]);

            $nbGame++;
        }
    }

    private function make_squads(Tournament $tournament)
    {
        $round = $tournament->rounds->last();
        $players = $this->getActivePlayers($tournament)->shuffle();
        $nb_squads = intval(ceil($players->count() / $tournament->squad_size));

        // Si on a un nombre d'équipe impaire,
        // On en rajoute une
        if ($nb_squads % 2 != 0) {
            // Si on peut dépasser la taille d'équipe, on enleve une équipe, sinon on en rajoute une,
            // ce qui va diminuer le nombre de joueurs par équipe
            $tournament->overflowable ? $nb_squads-- : $nb_squads++;
        }

        // Initialisation des squads
        $squads = [];
        for ($i = 0; $i < $nb_squads; $i++) {
            $squads[] = new Squad();
        }
        $round->squads()->saveMany($squads);
        $round->refresh();
        $squads = $round->squads;

        $s = 0;
        foreach ($players as $player) {
            $squads[$s]->players()->attach($player);

            $s = $s < $nb_squads - 1 ? $s + 1 : 0;
        }

        return $squads;
    }

    public function getActivePlayers(Tournament $tournament)
    {
        return $tournament->players()->where('enabled', true)->get();
    }

    public function updatePlayersScore(Squad $squad, int $score)
    {
        $squad->refresh();
        // On met à jour les score de chaques joueurs
        foreach ($squad->players as $player) {
            $player->score = 0;
            foreach ($player->squads as $squad) {
                foreach ($squad->games as $game) {
                    $player->score += $game->pivot->score;
                    $player->save();
                }
            }
        }
    }
}
