<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRoundRequest;
use App\Http\Requests\UpdateRoundRequest;
use App\Models\Game;
use App\Models\Round;
use App\Models\Squad;
use App\Models\Tournament;
use Inertia\Inertia;
use App\Services\TournamentsService;

class RoundController extends Controller
{
    public function __construct(private TournamentsService $ts)
    {
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreRoundRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoundRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Round  $round
     * @return \Illuminate\Http\Response
     */
    public function show(Tournament $tournament, Round $round)
    {
        $tournament->load('players');
        $tournament->load('rounds');
        $round->load('games');
        return Inertia::render("Rounds/Show", [
            "round" => $round,
            "tournament" => $tournament
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Round  $round
     * @return \Illuminate\Http\Response
     */
    public function edit(Round $round)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRoundRequest  $request
     * @param  \App\Models\Round  $round
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoundRequest $request, Round $round)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Round  $round
     * @return \Illuminate\Http\Response
     */
    public function destroy(Round $round)
    {
        //
    }

    // 
    public function reset(Round $round)
    {
        $squads = $round->squads;
        $games = $round->games;

        $squads->each(function ($squad) {
            $squad->players()->detach();
        });

        $games->each(function ($game) {
            $game->squads()->detach();
        });

        Squad::destroy($squads->pluck('id'));
        Game::destroy($games->pluck('id'));

        $round->refresh();
        $round = $this->ts->populate_round($round);
        $tournament = $round->tournament;

        return redirect(route('tournaments.round', [
            "tournament" => $tournament->id,
            "round" => $round->iteration
        ]));
    }
}
