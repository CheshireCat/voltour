<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\Squad;
use Illuminate\Http\Request;
use App\Services\TournamentsService;

class GameController extends Controller
{
    public function __construct(private TournamentsService $ts)
    {
    }
    //
    public function updateScore(Request $request, Game $game, Squad $squad)
    {
        $request->validate([
            "score" => ["numeric", "min:0", "max:254"],
        ]);
        $game->squads()->sync([$squad->id => ["score" => $request->score]], false);
        $this->ts->updatePlayersScore($squad, $request->score);
        return back();
    }
}
