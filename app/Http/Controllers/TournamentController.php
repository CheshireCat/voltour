<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePlayerRequest;
use App\Http\Requests\StoreTournamentRequest;
use App\Http\Requests\UpdateTournamentRequest;
use App\Models\Player;
use App\Models\Tournament;
use Inertia\Inertia;
use App\Services\TournamentsService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class TournamentController extends Controller
{
    public function __construct(private TournamentsService $tournamentsService)
    {
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tournaments = $this->tournamentsService->index();
        $tournaments->load('owner');
        $tournaments->load('players');
        return Inertia::render('Tournaments/Index', [
            "tournaments" => $tournaments
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTournamentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTournamentRequest $request)
    {
        $tournament = new Tournament();
        $tournament->name = $request->name;
        $tournament->squad_size = $request->squad_size;
        $tournament->start_at = new Carbon($request->start_at);

        /** @var \App\Models\User $user */
        $user = Auth::user();
        $tournament = $user->tournaments()->save($tournament);

        return redirect(route('tournaments.show', [
            "tournament" => $tournament
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tournament  $tournament
     * @return \Illuminate\Http\Response
     */
    public function show(Tournament $tournament)
    {

        // Si le tournoi est terminé, on renvoi au ranking
        if ($tournament->finished) {
            return redirect(route("tournaments.ranking", [
                "tournament" => $tournament
            ]));
        }

        // Si le tournoi a déjà un round en cours, on le charge
        if ($tournament->rounds->count() > 0) {
            return redirect(route("tournaments.round", [
                "tournament" => $tournament->id,
                "round" => $tournament->rounds->last()->iteration
            ]));
        }

        $tournament->load('players');
        $tournament->load('rounds');
        return Inertia::render('Tournaments/Show', [
            'tournament' => $tournament
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tournament  $tournament
     * @return \Illuminate\Http\Response
     */
    public function edit(Tournament $tournament)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTournamentRequest  $request
     * @param  \App\Models\Tournament  $tournament
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTournamentRequest $request, Tournament $tournament)
    {
        //
        $tournament->name = $request->name;
        $tournament->squad_size = $request->squad_size;
        $tournament->nb_courts = $request->nb_courts;
        $tournament->overflowable = $request->overflowable;
        $tournament->save();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tournament  $tournament
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tournament $tournament)
    {
        //
    }

    public function ranking(Tournament $tournament)
    {
        $tournament->load('players');
        $tournament->load('rounds');
        $tournament->players->load('squads.games');

        return Inertia::render("Tournaments/Ranking", [
            "tournament" => $tournament
        ]);
    }

    public function start(Tournament $tournament)
    {
        $this->tournamentsService->start($tournament);

        return redirect(route("tournaments.round", [
            "tournament" => $tournament->id,
            "round" => 1
        ]));
    }

    public function nextRound(Tournament $tournament)
    {
        $round = $this->tournamentsService->nextRound($tournament);

        return redirect(route("tournaments.round", [
            "tournament" => $tournament->id,
            "round" => $round
        ]));
    }

    public function close(Tournament $tournament)
    {
        $this->tournamentsService->close($tournament);

        return redirect(route("tournaments.ranking", [
            "tournament" => $tournament
        ]));
    }

    public function storePlayer(StorePlayerRequest $request, Tournament $tournament)
    {
        $player = new Player();
        $player->name = $request->name;

        $tournament->players()->save($player);

        return redirect()->back();
    }
}
