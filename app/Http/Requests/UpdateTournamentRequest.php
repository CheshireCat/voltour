<?php

namespace App\Http\Requests;

use App\Models\Tournament;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Unique;

class UpdateTournamentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $tournament = $this->route('tournament');
        return $tournament && $this->user()->can('update', $tournament);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $tournament = $this->route('tournament');
        return [
            "name" => ["required", Rule::unique('tournaments')->ignore($tournament)],
            "nb_courts" => ["nullable", "integer"],
            "squad_size" => ["nullable", "integer", "max:6"],
            "overflowable" => ["required", "boolean"]
            
        ];
    }
}
