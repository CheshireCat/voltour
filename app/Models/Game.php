<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use HasFactory;

    protected $fillable = ['name'];
    protected $with = ['squads'];

    public function round()
    {
        return $this->belongsTo(Round::class);
    }

    public function squads()
    {
        return $this->belongsToMany(Squad::class)->withPivot('score');
    }
}
