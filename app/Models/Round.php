<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Round extends Model
{
    use HasFactory;

    public function tournament()
    {
        return $this->belongsTo(Tournament::class);
    }

    public function squads()
    {
        return $this->hasMany(Squad::class);
    }

    public function games()
    {
        return $this->hasMany(Game::class);
    }
}
