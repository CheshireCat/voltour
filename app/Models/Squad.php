<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Squad extends Model
{
    use HasFactory;

    protected $with = ['players'];

    public function players()
    {
        return $this->belongsToMany(Player::class);
    }

    public function round()
    {
        return $this->belongsTo(Round::class);
    }

    public function games()
    {
        return $this->belongsToMany(Game::class)->withPivot('score');
    }
}
