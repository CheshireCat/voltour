<?php

namespace Database\Seeders;

use App\Models\Tournament;
use Database\Factories\TournamentFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TournamentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tournament::factory()
            ->count(10)
            ->hasPlayers(80)
            ->create();
    }
}
