<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Tournament>
 */
class TournamentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            "user_id" => 1,
            "slug" => $this->faker->slug(),
            "name" => $this->faker->catchPhrase(),
            "description" => $this->faker->paragraph(),
            "start_at" => $this->faker->dateTimeBetween('+1 day', '+3 days'),
            "end_at" => $this->faker->dateTimeBetween('+3 days', '+5 days')
        ];
    }
}
