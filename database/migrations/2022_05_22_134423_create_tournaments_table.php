<?php

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournaments', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class);
            $table->string('slug')->nullable();
            $table->string('name');
            $table->text('description')->nullable();
            $table->unsignedSmallInteger('squad_size')->default(6);
            $table->unsignedSmallInteger('nb_courts')->default(3);
            $table->boolean('overflowable')->default(false);
            $table->boolean('finished')->default(false);
            $table->timestamp('start_at')->default(Carbon::now());
            $table->timestamp('end_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournaments');
    }
};
